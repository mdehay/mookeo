/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

console.log('Hello Webpack Encore! Edit me in assets/app.js');
$( "#target" ).click(function() {
    $("#switch").html(
        "<div class=\"row\">\n" +
        "            <div class=\"col-md-6 offset-1\" style=\"margin-top: 50px;\">\n" +
        "                <span>\n" +
        "                    Identifier de nouvelles opportunités, améliorer ou imaginer de nouvelles solutions grâce au Design Thinking. Ce processus d’idéation apporte une vision pluridisciplinaire à vos projets à travers la co-création et propose\n" +
        "                    des méthodes itératives pour booster l’intelligence collective et adopter une vraie culture “design”. Venez découvrir le design thinking et animez des workshops au sein de vos équipes avec une approche centrée sur\n" +
        "                    l’utilisateur (UCD).\n" +
        "                </span>\n" +
        "                <br>\n" +
        "\n" +
        "            </div>\n" +
        "        </div>\n" +
        "        <div class=\"row\">\n" +
        "            <div class=\"col-md-10 offset-1\" style=\"margin-top: 50px;\">\n" +
        "                <img width=\"100%\" src=\"{{ asset('build/ux-indonesia-qC2n6RQU4Vw-unsplash.png') }}\">\n" +
        "            </div>\n" +
        "        </div>"
        +
        " <div class=\"row\">\n" +
        "                <div class=\"col-md-10 offset-1\" style=\"margin-top: 50px;\">\n" +
        "                    <h4>Formation animée en présentiel</h4>\n" +
        "                    <p>\n" +
        "                        La formation en présentiel se déroule sur 2 jours consécutifs.\n" +
        "                    </p>\n" +
        "                    <h4>Formation disponible en mode \"formation à distance\"</h4>\n" +
        "                    <p>\n" +
        "                        La formation à distance est décomposée en 4 demies journées, les matinées de 9h30 à 13h ou les après midi de 14h à 17h30.\n" +
        "                    </p>\n" +
        "                    <H1>OBJECTIFS</H1>\n" +
        "                    <ul>\n" +
        "                        <li>Découvrir et comprendre la méthodologie Design Thinking.</li>\n" +
        "                        <li>Identifier dans quels cas appliquer le Design Thinking.</li>\n" +
        "                        <li>Connaître les bénéfices et les limites de la méthodologie.</li>\n" +
        "                        <li>Adopter une approche centrée sur l’utilisateur.</li>\n" +
        "                        <li>S’approprier des outils de co-création et de créativité.</li>\n" +
        "                        <li>Animer un workshop de Design Thinking.</li>\n" +
        "                        <li>Mener le rôle de facilitateur créatif</li>\n" +
        "                    </ul>\n" +
        "                    <H1>PREREQUIS</H1>\n" +
        "                    <ul>\n" +
        "                        <li>Aucun</li>\n" +
        "                    </ul>\n" +
        "                    <H1>PUBLIC</H1>\n" +
        "                    <ul>\n" +
        "                        <li>Product Owners</li>\n" +
        "                        <li>Chef de projet/produit</li>\n" +
        "                        <li>Consultants</li>\n" +
        "                        <li>Dirigeants et entrepreneurs</li>\n" +
        "                        <li>Directeurs marketing</li>\n" +
        "                        <li>Designers</li>\n" +
        "                    </ul>\n" +
        "                    <H1>PEDAGOGIE</H1>\n" +
        "                    <p>50% théorie, 50% pratique</p>\n" +
        "                    <H1>FINANCEMENT EN FRANCE</H1>\n" +
        "                    <ul>\n" +
        "                        <li>Eligible au financement via OPCO (si la prise en charge couvre la totalité du coût de la formation)</li>\n" +
        "                        <li>Non éligible au financement via CPF</li>\n" +
        "                    </ul>\n" +
        "                    <H1>FORMATION EN INTER-ENTREPRISES</H1>\n" +
        "                    <p>Petit-déjeuners et déjeuners inclus dans le tarif</p>\n" +
        "                    <ul>\n" +
        "                        <li>9h00: Accueil petit-déjeuner</li>\n" +
        "                        <li>9h30: Début de la formation</li>\n" +
        "                    </ul>\n" +
        "                </div>\n" +
        "            </div>");
}
);
