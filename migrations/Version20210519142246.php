<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210519142246 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contained_formation (id INT AUTO_INCREMENT NOT NULL, index_list INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contained_formation_pack_formation (contained_formation_id INT NOT NULL, pack_formation_id INT NOT NULL, INDEX IDX_48BD8FFDBB087C1C (contained_formation_id), INDEX IDX_48BD8FFDADFBB483 (pack_formation_id), PRIMARY KEY(contained_formation_id, pack_formation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contained_formation_formation (contained_formation_id INT NOT NULL, formation_id INT NOT NULL, INDEX IDX_B9D9CA8BBB087C1C (contained_formation_id), INDEX IDX_B9D9CA8B5200282E (formation_id), PRIMARY KEY(contained_formation_id, formation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formation (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(60) NOT NULL, description VARCHAR(255) NOT NULL, formation_lien VARCHAR(255) NOT NULL, is_payant TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pack_formation (id INT AUTO_INCREMENT NOT NULL, groupe_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, age VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contained_formation_pack_formation ADD CONSTRAINT FK_48BD8FFDBB087C1C FOREIGN KEY (contained_formation_id) REFERENCES contained_formation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contained_formation_pack_formation ADD CONSTRAINT FK_48BD8FFDADFBB483 FOREIGN KEY (pack_formation_id) REFERENCES pack_formation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contained_formation_formation ADD CONSTRAINT FK_B9D9CA8BBB087C1C FOREIGN KEY (contained_formation_id) REFERENCES contained_formation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contained_formation_formation ADD CONSTRAINT FK_B9D9CA8B5200282E FOREIGN KEY (formation_id) REFERENCES formation (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contained_formation_pack_formation DROP FOREIGN KEY FK_48BD8FFDBB087C1C');
        $this->addSql('ALTER TABLE contained_formation_formation DROP FOREIGN KEY FK_B9D9CA8BBB087C1C');
        $this->addSql('ALTER TABLE contained_formation_formation DROP FOREIGN KEY FK_B9D9CA8B5200282E');
        $this->addSql('ALTER TABLE contained_formation_pack_formation DROP FOREIGN KEY FK_48BD8FFDADFBB483');
        $this->addSql('DROP TABLE contained_formation');
        $this->addSql('DROP TABLE contained_formation_pack_formation');
        $this->addSql('DROP TABLE contained_formation_formation');
        $this->addSql('DROP TABLE formation');
        $this->addSql('DROP TABLE pack_formation');
        $this->addSql('DROP TABLE `user`');
    }
}
