<?php

namespace App\Repository;

use App\Entity\ContainedFormation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContainedFormation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContainedFormation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContainedFormation[]    findAll()
 * @method ContainedFormation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContainedFormationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContainedFormation::class);
    }

    // /**
    //  * @return ContainedFormation[] Returns an array of ContainedFormation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContainedFormation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
