<?php

namespace App\Repository;

use App\Entity\PackFormation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PackFormation|null find($id, $lockMode = null, $lockVersion = null)
 * @method PackFormation|null findOneBy(array $criteria, array $orderBy = null)
 * @method PackFormation[]    findAll()
 * @method PackFormation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PackFormationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PackFormation::class);
    }

    // /**
    //  * @return PackFormation[] Returns an array of PackFormation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PackFormation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
