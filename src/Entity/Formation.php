<?php

namespace App\Entity;

use App\Repository\FormationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FormationRepository::class)
 */
class Formation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $Titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $FormationLien;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPayant;

    /**
     * @ORM\ManyToMany(targetEntity=ContainedFormation::class, mappedBy="Formation")
     */
    private $contained_for_formation;

    public function __construct()
    {
        $this->contained_for_formation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }
    
    public function getFormationLien(): ?string
    {
        return $this->FormationLien;
    }

    public function setFormationLien(string $FormationLien): self
    {
        $this->FormationLien = $FormationLien;

        return $this;
    }

    public function getIsPayant(): ?bool
    {
        return $this->isPayant;
    }

    public function setIsPayant(bool $isPayant): self
    {
        $this->isPayant = $isPayant;

        return $this;
    }

    /**
     * @return Collection|ContainedFormation[]
     */
    public function getContainedForFormation(): Collection
    {
        return $this->contained_for_formation;
    }

    public function addContainedForFormation(ContainedFormation $containedForFormation): self
    {
        if (!$this->contained_for_formation->contains($containedForFormation)) {
            $this->contained_for_formation[] = $containedForFormation;
            $containedForFormation->addFormation($this);
        }

        return $this;
    }

    public function removeContainedForFormation(ContainedFormation $containedForFormation): self
    {
        if ($this->contained_for_formation->removeElement($containedForFormation)) {
            $containedForFormation->removeFormation($this);
        }

        return $this;
    }
}
