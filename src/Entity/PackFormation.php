<?php

namespace App\Entity;

use App\Repository\PackFormationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PackFormationRepository::class)
 */
class PackFormation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Groupe_ID;

    /**
     * @ORM\ManyToMany(targetEntity=ContainedFormation::class, mappedBy="FK_PackFormation")
     */
    private $contained_for_packformation;

    public function __construct()
    {
        $this->contained_for_packformation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGroupeID(): ?int
    {
        return $this->Groupe_ID;
    }

    public function setGroupeID(int $Groupe_ID): self
    {
        $this->Groupe_ID = $Groupe_ID;

        return $this;
    }

    /**
     * @return Collection|ContainedFormation[]
     */
    public function getContainedForPackformation(): Collection
    {
        return $this->contained_for_packformation;
    }

    public function addContainedForPackformation(ContainedFormation $containedForPackformation): self
    {
        if (!$this->contained_for_packformation->contains($containedForPackformation)) {
            $this->contained_for_packformation[] = $containedForPackformation;
            $containedForPackformation->addFKPackFormation($this);
        }

        return $this;
    }

    public function removeContainedForPackformation(ContainedFormation $containedForPackformation): self
    {
        if ($this->contained_for_packformation->removeElement($containedForPackformation)) {
            $containedForPackformation->removeFKPackFormation($this);
        }

        return $this;
    }
}
