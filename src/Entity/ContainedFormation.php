<?php

namespace App\Entity;

use App\Repository\ContainedFormationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContainedFormationRepository::class)
 */
class ContainedFormation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=PackFormation::class, inversedBy="contained_for_packformation")
     */
    private $FK_PackFormation;

    /**
     * @ORM\ManyToMany(targetEntity=Formation::class, inversedBy="contained_for_formation")
     */
    private $Formation;

    /**
     * @ORM\Column(type="integer")
     */
    private $index_list;

    public function __construct()
    {
        $this->FK_PackFormation = new ArrayCollection();
        $this->Formation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|PackFormation[]
     */
    public function getFKPackFormation(): Collection
    {
        return $this->FK_PackFormation;
    }

    public function addFKPackFormation(PackFormation $fKPackFormation): self
    {
        if (!$this->FK_PackFormation->contains($fKPackFormation)) {
            $this->FK_PackFormation[] = $fKPackFormation;
        }

        return $this;
    }

    public function removeFKPackFormation(PackFormation $fKPackFormation): self
    {
        $this->FK_PackFormation->removeElement($fKPackFormation);

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormation(): Collection
    {
        return $this->Formation;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->Formation->contains($formation)) {
            $this->Formation[] = $formation;
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        $this->Formation->removeElement($formation);

        return $this;
    }

    public function getIndexList(): ?int
    {
        return $this->index_list;
    }

    public function setIndexList(int $index_list): self
    {
        $this->index_list = $index_list;

        return $this;
    }
}
