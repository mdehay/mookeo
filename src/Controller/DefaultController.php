<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index(): Response
    {
        return $this->render('payment/payment.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/", name="acceuil")
     */
    public function acceuil(): Response
    {
        return $this->render('/acceuil/acceuil.html.twig', [
            'controller_name' => 'AcceuilController',
        ]);
    }

    /**
     * @Route("/formation/design_thinking", name="dt")
     */
    public function design_thinking(): Response
    {
        return $this->render('/formation/design_thinking.html.twig', [
            'controller_name' => 'DtController',
        ]);
    }

    /**
     * @Route("/decouvrir", name="decouvrir")
     */
    public function decouvrir(): Response
    {
        return $this->render('/formation/decouvrir.html.twig', [
            'controller_name' => 'decouvrirController',
        ]);
    }

    /**
     * @Route("/blog", name="blog")
     */
    public function blog(): Response
    {
        return $this->render('/blog/blog.html.twig', [
            'controller_name' => 'blog',
        ]);
    }
}
