<?php

namespace App\Controller;

use App\Entity\Formation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormationController extends AbstractController
{
    /**
     * @Route("/", name="formation")
     */
    public function index(): Response
    {
        return $this->render('formation/index.html.twig', [
            'controller_name' => 'FormationController',
        ]);
    }

    /**
     * @Route("/formations", name="formation_list")
     */
    public function listFormation(): Response
    {

        $formations = $this->getDoctrine()->getRepository(Formation::class)->findAll();
        return $this->render('formation/list.html.twig', [
            'formations' => $formations
        ]);
    }

    /**
     * @Route("/formation/add", name="add_formation")
     */
    public function addFormation(Request $request): Response
    {

        $formation = new Formation();

        $form = $this->createFormBuilder($formation)
            ->add('titre', TextType::class) 
            ->add('description', TextType::class)
            ->add('formation_lien', FileType::class,[
                
                'label' => 'Formation (Video file)',
                
            ])
            ->add('is_payant',  CheckboxType::class, [
                'label'    => 'Show this entry publicly?',
                'required' => false,
            ])
            ->add('Enregistrer', SubmitType::class)
            ->getForm();

            $form->handleRequest($request);
            
        if ($form->isSubmitted() && $form->isValid()) {

  


            $file = $form->get('formation_lien')->getData();

            var_dump($formation);
            $upload_directory = $this->getParameter('upload_directory');
            $filename = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($upload_directory,$filename);

            $entityManager = $this->getDoctrine()->getManager();
            
            $formation->setFormationLien("MAVIDEO.MP4");

            $entityManager->persist($formation);
            $entityManager->flush();
            $formation = $form->getData();
            return $this->redirectToRoute('formation_list');
        }

        return $this->render('formation/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/formation/delete/{id}", name="delete_formation")
     */
    public function deleteFormation(int $id): Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        $formation = $entityManager->getRepository(Formation::class)->find($id);
        $entityManager->remove($formation);
        $entityManager->flush();
        return $this->redirectToRoute('formation_list');
    }
}
