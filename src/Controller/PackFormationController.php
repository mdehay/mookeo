<?php

namespace App\Controller;

use App\Entity\ContainedFormation;
use App\Entity\Formation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PackFormationController extends AbstractController
{
    /**
     * @Route("/pack", name="pack_formation")
     */
    public function index(): Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        $formation = $entityManager->getRepository(Formation::class)->find(4);

        $PackFormation = new ContainedFormation();
        $PackFormation->addFormation($formation);
        
        return $this->render('pack_formation/index.html.twig', [
            'controller_name' => 'SUCCES',
            'pack' => $PackFormation,
            'formation' => $formation,
        ]);
    }
}
