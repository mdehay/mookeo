<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Stripe\Stripe;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PaymentController extends AbstractController
{

    /**
     * @Route("/success", name="success")
     */
    public function success(): Response
    {

        return $this->render('payment/success.html.twig', []);
    }

    /**
     * @Route("/cancel", name="cancel")
     */
    public function cancel(): Response
    {

        return $this->render('payment/cancel.html.twig', []);
    }



    /**
     * @Route("/create-checkout-session", name="checkout")
     */
    public function checkout(): Response
    {

        \Stripe\Stripe::setApiKey('sk_test_51It7QXAxMDYorItqpESeuqTbhDcmZtieCjnyyAVBK2jgGi1Dbra50JKk46dJI3oQGfjeDiddAPOOz8rUg8ceiDvx00ymALNDPK');
        $session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
              'price_data' => [
                'currency' => 'eur',
                'product_data' => [
                  'name' => 'T shirt',
                ],
                'unit_amount' => 2000,
              ],
              'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $this->generateUrl('success',[],UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => $this->generateUrl('cancel',[],UrlGeneratorInterface::ABSOLUTE_URL),
          ]);

          return new JsonResponse(['id' => $session->id]);
 

    }
}
